package com.ruoyi.erp.controller;

import java.util.List;

import com.ruoyi.erp.domain.ErpStockOutLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpWorkProcessLog;
import com.ruoyi.erp.service.IErpWorkProcessLogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 生产进度日志Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpWorkProcessLog")
public class ErpWorkProcessLogController extends BaseController
{
    private String prefix = "erp/erpWorkProcessLog";

    @Autowired
    private IErpWorkProcessLogService erpWorkProcessLogService;

    @RequiresPermissions("erp:erpWorkProcessLog:view")
    @GetMapping()
    public String erpWorkProcessLog()
    {
        return prefix + "/erpWorkProcessLog";
    }

    /**
     * 查询生产进度日志列表
     */
    @RequiresPermissions("erp:erpWorkProcessLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpWorkProcessLog erpWorkProcessLog)
    {
        startPage();
        List<ErpWorkProcessLog> list = erpWorkProcessLogService.selectErpWorkProcessLogList(erpWorkProcessLog);
        return getDataTable(list);
    }

    /**
     * 导出生产进度日志列表
     */
    @RequiresPermissions("erp:erpWorkProcessLog:export")
    @Log(title = "生产进度日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpWorkProcessLog erpWorkProcessLog)
    {
        List<ErpWorkProcessLog> list = erpWorkProcessLogService.selectErpWorkProcessLogList(erpWorkProcessLog);
        ExcelUtil<ErpWorkProcessLog> util = new ExcelUtil<ErpWorkProcessLog>(ErpWorkProcessLog.class);
        return util.exportExcel(list, "erpWorkProcessLog");
    }

    /**
     * 新增生产进度日志
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存生产进度日志
     */
    @RequiresPermissions("erp:erpWorkProcessLog:add")
    @Log(title = "生产进度日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpWorkProcessLog erpWorkProcessLog)
    {
        return toAjax(erpWorkProcessLogService.insertErpWorkProcessLog(erpWorkProcessLog));
    }

    /**
     * 修改生产进度日志
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpWorkProcessLog erpWorkProcessLog = erpWorkProcessLogService.selectErpWorkProcessLogById(id);
        mmap.put("erpWorkProcessLog", erpWorkProcessLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存生产进度日志
     */
    @RequiresPermissions("erp:erpWorkProcessLog:edit")
    @Log(title = "生产进度日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpWorkProcessLog erpWorkProcessLog)
    {
        return toAjax(erpWorkProcessLogService.updateErpWorkProcessLog(erpWorkProcessLog));
    }

    /**
     * 删除生产进度日志
     */
    @RequiresPermissions("erp:erpWorkProcessLog:remove")
    @Log(title = "生产进度日志", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpWorkProcessLogService.deleteErpWorkProcessLogByIds(ids));
    }

    /**
     * 查看详细
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        ErpWorkProcessLog erpWorkProcessLog = erpWorkProcessLogService.selectErpWorkProcessLogById(id);
        mmap.put("erpWorkProcessLog", erpWorkProcessLog);
        return prefix + "/detail";
    }
}

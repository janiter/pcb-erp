package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpModelMapper;
import com.ruoyi.erp.domain.ErpModel;
import com.ruoyi.erp.service.IErpModelService;
import com.ruoyi.common.core.text.Convert;

/**
 * 生产型号Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpModelServiceImpl implements IErpModelService 
{
    @Autowired
    private ErpModelMapper erpModelMapper;

    /**
     * 查询生产型号
     * 
     * @param id 生产型号ID
     * @return 生产型号
     */
    @Override
    public ErpModel selectErpModelById(String id)
    {
        return erpModelMapper.selectErpModelById(id);
    }

    /**
     * 查询生产型号列表
     * 
     * @param erpModel 生产型号
     * @return 生产型号
     */
    @Override
    public List<ErpModel> selectErpModelList(ErpModel erpModel)
    {
        return erpModelMapper.selectErpModelList(erpModel);
    }

    /**
     * 新增生产型号
     * 
     * @param erpModel 生产型号
     * @return 结果
     */
    @Override
    public int insertErpModel(ErpModel erpModel)
    {
        erpModel.setId(IdUtils.fastSimpleUUID());
        erpModel.setCreateTime(DateUtils.getNowDate());
        return erpModelMapper.insertErpModel(erpModel);
    }

    /**
     * 修改生产型号
     * 
     * @param erpModel 生产型号
     * @return 结果
     */
    @Override
    public int updateErpModel(ErpModel erpModel)
    {
        erpModel.setUpdateTime(DateUtils.getNowDate());
        return erpModelMapper.updateErpModel(erpModel);
    }

    /**
     * 删除生产型号对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpModelByIds(String ids)
    {
        return erpModelMapper.deleteErpModelByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除生产型号信息
     * 
     * @param id 生产型号ID
     * @return 结果
     */
    @Override
    public int deleteErpModelById(String id)
    {
        return erpModelMapper.deleteErpModelById(id);
    }
}
